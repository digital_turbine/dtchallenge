package com.mattrea.dtchallenge.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mattrea.dtchallenge.R;
import com.mattrea.dtchallenge.model.Ad;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Custom recyclerview adapter to display Photo objects. Uses picasso to download and display images
 * Defines OnItemClickListener interface that is implemented in Fragments that use this adapter
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{

    private static final String TAG = "AlbumAdapter";
    private List<Ad> adList;
    private Context mContext;
    private LayoutInflater layoutInflater;
    private OnItemClickListener listener;

    public ProductAdapter(List<Ad> adList){
        this.adList = adList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        layoutInflater = LayoutInflater.from(mContext);

        View item;
        item = layoutInflater.inflate(R.layout.cardview, parent, false);
        ViewHolder holder = new ViewHolder(item, listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Ad ad = adList.get(position);

        holder.productName.setText(ad.getProductName());
        holder.productDescription.setText(ad.getProductDescription());
        holder.numRatings.setText(ad.getNumberOfRatings().trim());
        // parse the URL
        Uri thumbnail = Uri.parse(ad.getProductThumbnail());
        Uri rating = Uri.parse(ad.getAverageRatingImageURL());

        Picasso.with(mContext).load(thumbnail)
                .fit()
                .centerInside()
                .into(holder.thumbnail);

        Picasso.with(mContext).load(rating)
                .fit()
                .centerInside()
                .into(holder.rating);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return adList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {
        public CardView cardview;
        TextView productName;
        TextView productDescription;
        TextView numRatings;
        public ImageView thumbnail;
        public ImageView rating;
        private OnItemClickListener listener;

        public ViewHolder(View itemView, OnItemClickListener l) {
            super(itemView);
            cardview = (CardView)itemView.findViewById(R.id.cv);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            rating = (ImageView) itemView.findViewById(R.id.rating);
            productName = (TextView)itemView.findViewById(R.id.productName);
            productDescription = (TextView)itemView.findViewById(R.id.productDescription);
            numRatings = (TextView)itemView.findViewById(R.id.numRatings);
            listener = l;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null)
                        listener.onItemClick(getPosition());
                }
            });

        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }
}
