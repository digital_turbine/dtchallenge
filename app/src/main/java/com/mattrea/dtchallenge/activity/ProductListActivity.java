package com.mattrea.dtchallenge.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.mattrea.dtchallenge.DTApplication;
import com.mattrea.dtchallenge.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/*  Simple activity that displays embedded fragments.
 *  The layout contains a toolbar in a coordinatorlayout
 */
public class ProductListActivity extends AppCompatActivity {

    private static final String TAG = "AlbumActivity";
    private static final String USER_ID = "1";

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.progress_bar)
    protected ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((DTApplication) getApplication()).getComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_placeholder, ProductListFragment.newInstance());
        ft.commit();
    }

    public void startProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void stopProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }
}