package com.mattrea.dtchallenge.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mattrea.dtchallenge.DTApplication;
import com.mattrea.dtchallenge.R;
import com.mattrea.dtchallenge.adapter.ProductAdapter;
import com.mattrea.dtchallenge.model.Ad;
import com.mattrea.dtchallenge.model.Ads;
import com.mattrea.dtchallenge.network.ProductAPI;
import com.mattrea.dtchallenge.network.RestClient;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Matt on 8/19/16.
 */

/*
 *  This fragment displays all of the current user's Albums in a grid
 *  Each album's cover photo is shown in the grid
 *  Each cover photo is clickable. On click, the Album Details screen is shown
 *  For now, the user is always id = 1. In the future, a login screen with authentication will be added
 *
 */
public class ProductListFragment extends Fragment {
    private static final String TAG = "ProductListFragment";

    private ProductAdapter productAdapter;
    private ArrayList<Ad> productList;

    @Inject
    protected RestClient restClient;
    protected ProductAPI mApi;

    @BindView(R.id.recyclerview)    protected RecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((DTApplication) getActivity().getApplication()).getComponent().inject(this);
        super.onCreate(savedInstanceState);
        mApi = restClient.create(ProductAPI.class);
    }

    public static ProductListFragment newInstance(){
        ProductListFragment fragment = new ProductListFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.recyclerview, container, false);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this,view);

        initUI();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.toolbar_title_product_list));

        getAlbum(getString(R.string.last_name));
        return view;
    }

    private void initUI(){
        productList = new ArrayList<>();
        productAdapter = new ProductAdapter(productList);
        productAdapter.setOnItemClickListener(new ProductAdapter.OnItemClickListener(){
            public void onItemClick(int position){
                final String appPackageName = productList.get(position).getAppId(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        recyclerView.setHasFixedSize(true);
//        recyclerView.addItemDecoration(new SpacesItemDecoration((int) getActivity().getResources().getInteger(R.integer.recyclerview_spacing)));
        recyclerView.setAdapter(productAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    }

    public void getAlbum(String name) {
        Log.d(TAG,"Sending Request");
        final String finalName = name;
        ((ProductListActivity)getActivity()).startProgress();
        mApi.getAds()
                .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Ads>() {
                    @Override
                    public void call(Ads ads) {
                        productList.clear();
//                        productList = (ArrayList<Ad>) ads.adList;
                        productList.addAll(ads.adList);
                        productAdapter.notifyDataSetChanged();
                        Log.d(TAG,"got the ad list");
                        ((ProductListActivity)getActivity()).stopProgress();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d(TAG,"error while getting ads");
                        ((ProductListActivity)getActivity()).stopProgress();
                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle(getResources().getString(R.string.dialog_title_error));
                        alertDialog.setMessage(getResources().getString(R.string.server_error));
                        alertDialog.setCanceledOnTouchOutside(false);
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Retry",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        getAlbum(finalName);
                                    }
                                });
                        alertDialog.show();

                    }
                });
    }
}
