package com.mattrea.dtchallenge;

import com.mattrea.dtchallenge.activity.*;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Defines injections at the application level
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(ProductListFragment fragment);
    void inject(ProductListActivity activity);
}
