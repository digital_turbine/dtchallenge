package com.mattrea.dtchallenge.network;

import com.mattrea.dtchallenge.model.Ads;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Matt on 8/22/16.
 */
public interface ProductAPI {
    @GET("/getAds?id=236&password=OVUJ1DJN&siteId=4288&deviceId=4230&sessionId=techtestsession&totalCampaignsRequested=10&lname=Rea")
    Observable<Ads> getAds();
}
