package com.mattrea.dtchallenge.network;

import com.mattrea.dtchallenge.model.Ad;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class RestClient {
    private static final String BASE_URL = "http://ads.appia.com/";
    private static final String TAG = RestClient.class.toString();
    private final Retrofit mClient;

    public RestClient() {
        OkHttpClient ok = new OkHttpClient.Builder().build();

        mClient = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(ok)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    /**
     * Creates an implementation of the API defined by the ApiInterfaceClass
     * @param apiInterfaceClass
     * @param <T>
     * @return
     */
    public <T> T create(Class<T> apiInterfaceClass) {
        return mClient.create(apiInterfaceClass);
    }
}
