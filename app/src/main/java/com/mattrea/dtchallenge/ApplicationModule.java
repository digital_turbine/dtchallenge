package com.mattrea.dtchallenge;

import com.mattrea.dtchallenge.network.RestClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Things that live for the duration of the application and will be injected into whatever the
 * component defines
 */
@Module
public class ApplicationModule {
    @Provides
    @Singleton
    RestClient provideRestService() {
        return new RestClient();
    }
}
