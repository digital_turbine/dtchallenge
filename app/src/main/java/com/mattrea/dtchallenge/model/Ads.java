package com.mattrea.dtchallenge.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Matt on 8/22/16.
 */
@Root(name = "ads")
public class Ads {
    @ElementList(inline = true)
    public List<Ad> adList;
    @Element
    public String responseTime;
    @Element
    public String serverId;
    @Element
    public String totalCampaignsRequested;
    @Element
    public String version;
}